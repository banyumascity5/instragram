import React, {Component} from 'react';
import {View,ScrollView} from 'react-native';
import {Header,Story,Halaman,Futter} from './index';

class App extends Component {
  render () {
    return (
      <View>
        <Header />
        <ScrollView>
        <Story />
        <Halaman />
        </ScrollView>
        </View>
        
    
    );
  }
}

export default App; 
  