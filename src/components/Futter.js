import React from 'react'
import { View, StyleSheet,Image} from 'react-native'
import  Icon  from 'react-native-vector-icons/Ionicons';
import  Icon1  from 'react-native-vector-icons/Feather'


const Futter = () => {
    return (
        <View style={Setting.navBar}>
         <Icon style={Setting.NavItem} name="home" size={30} />
         <Icon style={Setting.NavItem} name="search-outline" size={30} />
         <Icon style={Setting.NavItem} name="duplicate-outline" size={30} />
         <Icon1 style={Setting.NavItem} name="shopping-bag" size={30} />
         <Image style={{width:35,height:35,borderRadius:20,marginVertical:10,marginHorizontal:5}} source={require('../assest/tk.jpg')}/>
        </View>
    )
}

const Setting = StyleSheet. create({
    navBar: {
        height:55,
        backgroundColor:'white',
        paddingHorizontal: 3,
        flexDirection:'row',
    },
    NavItem: {
        marginLeft:30
    }

})

export default Futter
