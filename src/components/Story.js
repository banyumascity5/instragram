import React from 'react';
import {View, Text, ScrollView, Image,} from  'react-native';

const Story =() => {
    return (
        <View>
            <ScrollView horizontal={true}
            showsHorizontalScrollIndicator={false} style={{height:110,backgroundColor: 'white',paddingHorizontal:15}}>
                <Image source={require('../assest/cd.jpg')} 
                style={{height: 70,width: 75,borderRadius:60,marginTop:20,marginHorizontal:5}} />
                <Image source={require('../assest/kp.jpeg')} 
                style={{height: 70,width: 75,borderRadius:60,marginTop:20,marginHorizontal:5}} />
                <Image source={require('../assest/nb.jpg')} 
                style={{height: 70,width: 75,borderRadius:60, marginTop:20,marginHorizontal:5}} />
                <Image source={require('../assest/nr.jpg')} 
                style={{ height: 70,width: 75, borderRadius:60,marginTop:20,marginHorizontal:5}} />
                <Image source={require('../assest/pm.jpg')} 
                style={{height: 70,width: 75,borderRadius:60, marginTop:20,marginHorizontal:5}} />
                <Image source={require('../assest/tk.jpg')} 
                style={{height: 70,width: 75, borderRadius:60, marginTop:20,marginHorizontal:5}} />
          </ScrollView>
        </View>
    )
}

export default Story